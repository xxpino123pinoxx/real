<?


add_image_size('size1',60,60,true);
add_image_size('size2',100,100,true);
add_image_size('size3',197,125,true);

function do_post_thumbnail_feeds($content) {
    global $post;
    if(has_post_thumbnail($post->ID)) {
        $content = '<div>' . get_the_post_thumbnail($post->ID) . '</div>' . $content;
    }
    return $content;
}
add_filter('the_excerpt_rss', 'do_post_thumbnail_feeds');
add_filter('the_content_feed', 'do_post_thumbnail_feeds');

if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );

set_post_thumbnail_size(129,129,true);



/* ページネーション
============================================= */
function pagination($pages = '', $range = 1){
	$showitems = ($range * 1)+1;//表示するページ数（５ページを表示）

      global $paged;//現在のページ値
      if(empty($paged)) $paged = 1;//デフォルトのページ


      if($pages == '')
      {
          global $wp_query;
          $pages = $wp_query->max_num_pages;//全ページ数を取得

       if(!$pages)//全ページ数が空の場合は、１とする
          {
              $pages = 1;
          }
      }

      if(1 != $pages)//全ページが１でない場合はページネーションを表示する
			{
echo "<ul class=\"pagenation mt50\">\n";
//Prev：現在のページ値が１より大きい場合は表示
          if($paged > 1) echo "<li class=\"prev\"><a href='".get_pagenum_link($paged - 1)."'>&lt;</a></li>\n";

          for ($i=1; $i <= $pages; $i++)
          {
              if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
              {
                 //三項演算子での条件分岐
                 echo ($paged == $i)? "<li><span class=\"current\">".$i."</span></li>\n":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
              }
          }
//Next：総ページ数より現在のページ値が小さい場合は表示
if ($paged < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\">&gt;</a></li>\n";
echo "</ul>\n";
      }
}








/* カスタム投稿
============================================= */



/*カスタム投稿*/
register_post_type(
  'model', 
  array(
    'label' => 'タレント一覧',
    'description' => 'タレント一覧',
    'menu_position' => 5,
    'public' => true,
    'query_var' => true,
    //'has_archive' => true,
    'rewrite' => array('slug' => 'models'),
    //'yarpp_support' => true,
    'supports' => array(
      'title',
      'editor',
      //'excerpt',
      'author',
      'thumbnail',
      
    )
  )
);

//カスタムタクソノミー（カテゴリ）
   register_taxonomy(
        'model-cat',
        'model',
        array(
'hierarchical' => true, 
'rewrite' => array('slug' => 'models','pages' => true),
            'label' => 'カテゴリ',
            'singular_label' => 'カテゴリ',
  'show_admin_column' => true
        )
    );
//
add_rewrite_rule('models/([^/]+)/page/([0-9]+)/?$', 'index.php?model-cat=$matches[1]&paged=$matches[2]', 'top');
add_rewrite_rule('models/([^/]+)/page/([0-9]+)/?$', '?model-cat=$matches[1]&paged=$matches[2]', 'top');



/*カスタム投稿*/
register_post_type(
  'work', 
  array(
    'label' => '実績',
    'description' => '実績',
    'menu_position' => 6,
    'public' => true,
    'query_var' => true,
    //'has_archive' => true,
    //'yarpp_support' => true,
    'supports' => array(
      'title',
      'editor',
      //'excerpt',
      'author',
      'thumbnail',
      
    )
  )
);

//カスタムタクソノミー（カテゴリ）
  register_taxonomy(
        'work-cat',
        'work',
        array(
'hierarchical' => true, 
'rewrite' => array('slug' => 'works'),
            'label' => 'カテゴリ',
            'singular_label' => 'カテゴリ',
  'show_admin_column' => true
        )
    );


/*カスタム投稿*/
add_action( 'init', 'create_post_type3' ); 
function create_post_type3() { 
  register_post_type( 'audition', /* post-type */
    array( 
      'labels' => array( 
        'name' => __( 'オーディション' ), 
        'singular_name' => __( 'オーディション' ) 
      ), 
      'public' => true, 
      'supports' => array( 'title', 'thumbnail', 'author','editor' ), 
      'menu_position' =>7,
      'has_archive' => false,
	  
	  
    ) 
  ); 
        /* ここから 
  register_taxonomy(
    'audition-cat',
    'audition', 
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      //'public' => true,
      'show_ui' => true
    )
  );
/* ここまでを追加 */ 

flush_rewrite_rules( false );
}




//
add_rewrite_rule('works/([^/]+)/page/([0-9]+)/?$', 'index.php?work-cat=$matches[1]&paged=$matches[2]', 'top');
add_rewrite_rule('works/([^/]+)/page/([0-9]+)/?$', '?work-cat=$matches[1]&paged=$matches[2]', 'top');





// ↓シングルのあるカスタム投稿の場合のコード↓
/* 土地情報 */
/*register_post_type(
	'land', 
	array(
		'label' => '土地情報',
		'description' => '土地情報',
		'menu_position' => 5,
		'public' => true,
		'query_var' => false,
		'has_archive' => true,
		'rewrite' => array('slug' => 'concept/land'),
		'yarpp_support' => true,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			
		)
	)
);*/

//カスタムタクソノミー（カテゴリ）
  /*  register_taxonomy(
        'land-cat',
        'land',
        array(
'hierarchical' => true, 
'rewrite' => array('slug' => 'concept/land'),
            'label' => 'カテゴリ',
            'singular_label' => '土地情報カテゴリ',
	'show_admin_column' => true
        )
    );*/
//
//add_rewrite_rule('concept/land/([^/]+)/page/([0-9]+)/?$', 'index.php?land-cat=$matches[1]&paged=$matches[2]', 'top');
/*add_rewrite_rule('concept/land/([^/]+)/page/([0-9]+)/?$', '?land-cat=$matches[1]&paged=$matches[2]', 'top');*/

/* 施工事例 */
/*register_post_type(
	'gallery', 
	array(
		'label' => '施工事例',
		'description' => '施工事例',
		'menu_position' => 5,
		'hierarchical' => true,
		'rewrite' => true,
		'public' => true,
		'has_archive' => true,
		'yarpp_support' => true,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			
		)
	)
);*/
//カスタムタクソノミー（カテゴリ）
 /*   register_taxonomy(
        'gallery-cat',
        'gallery',
        array(
	'rewrite' => array('slug' => 'gallery','pages' => true),
'hierarchical' => true,
            'label' => 'カテゴリ',
            'singular_label' => '施工事例カテゴリ',
	'show_admin_column' => true
        )
    );*/
//
//add_rewrite_rule('gallery/([^/]+)/page/([0-9]+)/?$', '?gallery-cat=$matches[1]&paged=$matches[2]', 'top');
////add_rewrite_rule('gallery/([^/]+)/page/([0-9]+)/?$', 'index.php?gallery-cat=$matches[1]&paged=$matches[2]', 'top');

//ユーザーエージェント
function is_mobile(){
    $useragents = array(
        'iPhone', // iPhone
        'iPod', // iPod touch
        'Android', // 1.5+ Android
        'dream', // Pre 1.5 Android
        'CUPCAKE', // 1.5+ Android
        'blackberry9500', // Storm
        'blackberry9530', // Storm
        'blackberry9520', // Storm v2
        'blackberry9550', // Storm v2
        'blackberry9800', // Torch
        'webOS', // Palm Pre Experimental
        'incognito', // Other iPhone browser
        'webmate' // Other iPhone browser
    );
    $pattern = '/'.implode('|', $useragents).'/i';
    return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

//ユーザーエージェント ipad
function is_ipad(){
    $useragents = array(
        'ipad', // ipad
    );
    $pattern = '/'.implode('|', $useragents).'/i';
    return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}


/* ページタイトルを返す関数
============================================= */
function get_title($id){

  if(is_home()){
    $title = "株式会社レアル［REAL］";

  }else if (is_single()){
    $gettitle = the_title( '' , '｜' , false ); 
    $title = $gettitle."株式会社レアル［REAL］";

  }else if (is_category()){
    $cat_now = get_the_category();
    $cat_now = $cat_now[0];
    $now_name = $cat_now->cat_name;
    $title = $now_name."｜株式会社レアル［REAL］";

  }else if (is_post_type_archive("land")){  
    $title = "伊達・室蘭・登別で家を建てるオススメエリア情報｜株式会社レアル［REAL］";
  //}else if (is_post_type_archive("gallery")){ 
    //$title = "新築のプランニングに役立つ間取りと施工例｜隠口-こもりく-";
    //
    //
  }else if (is_404()){  
    $title = "お探しのページが見つかりません｜株式会社レアル［REAL］";


  }else if (is_page('models')){  
    $title = "タレント一覧｜株式会社レアル［REAL］";

  }else if (is_page('campaign-girl')){  
    $title = "キャンペーンガール｜株式会社レアル［REAL］";

  }else if (is_page('audition')){  
    $title = "オーディション｜株式会社レアル［REAL］";

  }else if (is_page('entry')){  
    $title = "エントリーフォーム｜株式会社レアル［REAL］";

  }else if (is_page('works')){  
    $title = "実績｜株式会社レアル［REAL］";

  }else if (is_page('company')){  
    $title = "会社概要｜株式会社レアル［REAL］";
    
  }else if (is_page('guideline')){  
    $title = "モデル使用に関するガイドライン｜株式会社レアル［REAL］";

  }else if (is_page('contact')){  
    $title = "お問い合わせ｜株式会社レアル［REAL］";


  }else if (is_page('privacy')){  
    $title = "個人情報保護方針｜株式会社レアル［REAL］";

  }else if (is_page('sitemaps')){  
    $title = "サイトマップ｜株式会社レアル［REAL］";

  }else if (is_archive()){
    $my_term   = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_name  = $my_term->name;
    $title = $term_name."｜株式会社レアル［REAL］";
    if ($term_name != ""){
      if($term_name == "リフォーム施工事例"){ $title = "伊達・室蘭・登別でのリフォーム事例｜株式会社レアル［REAL］";}
      if($term_name == "新築施工事例"){         $title = "伊達・室蘭・登別での新築家づくり事例｜株式会社レアル［REAL］";}
    }else{
    $title = get_query_var('monthnum')."月のお知らせ｜株式会社レアル［REAL］";
    }
  }

    return $title;

}


/* デスクリプションを返す関数
============================================= */
function get_desc($id){

  if(is_home()){
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。イベントコンパニオンやキャンペーンガールも多数在籍。日本モデルエージェンシー協会に加盟しており、在籍モデルはファッションショー・雑誌・テレビなど様々なメディアで活躍しています。";

  }else if (is_single()){
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。在籍モデルはファッションショー・ファッション雑誌・テレビ番組・CM・ブライダルなど様々なメディアで活躍しています。最新の情報はこちらのページでご確認ください。";

  }else if (is_category()){
    $cat_now = get_the_category();
    $cat_now = $cat_now[0];
    $now_name = $cat_now->cat_name;
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。在籍モデルはファッションショー・ファッション雑誌・テレビ番組・CM・ブライダルなど様々なメディアで活躍しています。最新の情報はこちらのページでご確認ください。";

  /*}else if (is_post_type_archive("land")){  
    $desc = "家を建てるなら暮らしやすい地域選びも重要。このページでは土屋建設が人気の地域情報をご紹介。住みやすいエリアを選んだら、家づくりがどんどん具体化していきますよ。";
  }else if (is_post_type_archive("gallery")){ 
    $desc = "伊達・室蘭・登別の建設会社 土屋建設の新築・リフォーム施工例はこちら。家づくりのプランニングに役立つ間取り図と施工例ギャラリーをご紹介。自然素材にこだわった本当に良い家をご紹介しています。";*/
	
	
	
  }else if (is_page('models')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。メンズモデルや外国出身のモデルも在籍。ファッションショー・ファッション雑誌・テレビ番組・CM・ブライダル・カタログなど幅広いメディアで活躍しています。";

  }else if (is_page('campaign-girl')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。イベントコンパニオンやキャンペーンガール、レースクイーンも多数在籍。司会やアンケート・サンプリング調査にも対応可能です。旭川、北見、帯広、釧路、函館など道内全域へ派遣いたします。";

  }else if (is_page('audition')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。日本モデルエージェンシー協会加盟で、在籍モデルはファッションショー・雑誌・テレビなど幅広く活躍しています。オーディションもレッスンも無料ですので、興味がある方はぜひご参加ください。";

  }else if (is_page('works')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。在籍モデルは、テレビCM・ファッションショー・ファッション雑誌・テレビ番組・ブライダルなど様々なメディアで活躍しています。最新の実績の一部はこちらのページでご確認いただけます。";

  }else if (is_page('company')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。東京にもオフィスを構え、モデルやイベントコンパニオンのマネージメントや養成の他、ファッションショー・セールスプロモーションイベント・コンサートの企画・制作・運営も行っています。";
    
  }else if (is_page('guideline')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。日本モデルエージェンシー協会に加盟しており、モデルの肖像財産権（パブリシティ権）の擁護・維持・確立のための啓蒙活動に積極的に取り組んでいます。";

  }else if (is_page('contact')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。モデル、イベントコンパニオン、キャンペーンガール、司会の派遣等についてはこちらからお問い合わせください。旭川、北見、帯広、釧路、函館等の道内はもちろん道外への派遣にも対応いたします。";

  }else if (is_page('privacy')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。弊社は個人情報保護に関する基本方針を定め、個人情報保護法の遵守や情報の管理を徹底を行っています。";

  }else if (is_page('sitemaps')){  
    $desc = "MODEA（モーディア）は北海道・札幌のモデル事務所です。イベントコンパニオンやキャンペーンガールも多数在籍。日本モデルエージェンシー協会に加盟しており、在籍モデルはファッションショー・雑誌・テレビなど様々なメディアで活躍しています。";




  }else if (is_archive()){
    $my_term   = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_name  = $my_term->name;
    $desc = $term_name."MODEA（モーディア）は北海道・札幌のモデル事務所です。在籍モデルはファッションショー・ファッション雑誌・テレビ番組・CM・ブライダルなど様々なメディアで活躍しています。最新の情報はこちらのページでご確認ください。";
    if ($term_name != ""){
      if($term_name == "リフォーム施工事例"){ $desc = "伊達・室蘭・登別の建設会社 土屋建設のリフォーム施工例はこちら。あなたの持ち家をまるで新築のようにリフォーム。ライフスタイルにぴったりの理想の家にリフォーム致します。";}
      if($term_name == "新築施工事例"){         $desc = "伊達・室蘭・登別の建設会社 土屋建設の新築施工例はこちら。薪ストーブがよく合うぬくもりの家づくりをご提案。細部までこだわった「本物の木の家」をぜひご覧ください。";}
    }else{
    $desc = "";
    }
  }

    return $desc;

}

/* キーワードを返す関数
============================================= */
function get_keyword($id){

	if(is_home()){
		$keyword = "北海道,札幌,モデル,派遣,キャンペーンガール,イベントコンパニオン,事務所";

	/*}else if (is_post_type_archive("land")){	
		$keyword = "伊達,家を建てる";
	}else if (is_post_type_archive("gallery")){	
		$keyword = "新築,間取り";*/

	}else if (is_archive()){
		$my_term   = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$term_name  = $my_term->name;
		if ($term_name != ""){
			if($term_name == "リフォーム施工事例"){ $keyword = "伊達,リフォーム,事例";}
			if($term_name == "新築施工事例"){         $keyword = "伊達,新築,事例";}
		}else{
		$keyword = "";
		}
	}

		return $keyword;

}

/*カスタムメニューを有効化*/
//add_theme_support( 'menus' );
register_nav_menus( array(
    'gloval_nav' => 'グローバルナビゲーション',
    'footer_nav' => 'フッターナビゲーション',
));


//固定ページではビジュアルエディタを利用できないようにする
function disable_visual_editor_in_page(){
  global $typenow;
  if( $typenow == 'page' ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}
function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );

/*メニューの項目名を変更*/
add_filter(  'gettext',  'change_side_text'  );
add_filter(  'ngettext',  'change_side_text'  );
function change_side_text( $translated ) {
  $translated = str_ireplace(  'ダッシュボード',  '管理画面TOP',  $translated );
  $translated = str_ireplace(  'メディア',  '画像管理',  $translated );
  $translated = str_ireplace(  'Subscribe2',  '読者管理',  $translated );
  return $translated;
}

/*ダッシュボード（管理画面TOP）の不要なウィジェットを削除*/
function example_remove_dashboard_widgets() {
global $wp_meta_boxes;
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');



//ファビコンを表示する
function blog_favicon() {
  echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_bloginfo('template_url').'/img/favicon.ico" />'."\n";
}
add_action('wp_head', 'blog_favicon');



//管理画面にもファビコンをつけたい
function admin_favicon() {
  echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_bloginfo('template_url').'/img/favicon.ico" />';
}
add_action('admin_head', 'admin_favicon');


//

function my_form_tag_filter($tag){
	if ( ! is_array( $tag ) )
		return $tag;

	if ( isset( $_GET['postID'] ) ) {
		$name = $tag['name'];
		if ( $name == 'event-title' ) {
			$postID = $_GET['postID'];
			$mailsubject = get_post( $postID, ARRAY_A );
			$tag['values'] = (array)$mailsubject['post_title'];
		}
	}
	return $tag;
}
add_filter('wpcf7_form_tag', 'my_form_tag_filter', 11);


//記事内の画像の1枚目を抽出する
function getPostImage($mypost){
if(empty($mypost)){ return(null); }
if(ereg('<img([ ]+)([^>]*)src\=["|\']([^"|^\']+)["|\']([^>]*)>',$mypost->post_content,$img_array)){
  
// imgタグで直接画像にアクセスしているものがある
$dummy=ereg('<img([ ]+)([^>]*)alt\=["|\']([^"|^\']+)["|\']([^>]*)>',$mypost->post_content,$alt_array);
$resultArray["url"] = $img_array[3];
$resultArray["alt"] = $alt_array[3];
}else{
  
// 直接imgタグにてアクセスされていない場合は紐づけられた画像を取得
$files = get_children(array('post_parent' => $mypost->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image'));
if(is_array($files) && count($files) != 0){
$files=array_reverse($files);
$file=array_shift($files);
$resultArray["url"] = wp_get_attachment_url( $attachment->ID, 'thumbnail' );;
$resultArray["alt"] = $file->post_title;
}else{
return(null);
}
}
return($resultArray);
}

//ループ回数を取得
function loopNumber(){
global $wp_query;
return $wp_query->current_post+1;
}

?>
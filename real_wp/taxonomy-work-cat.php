<?php get_header(); ?>

<div class="containerAll">

<div class="container100p2">


<div class="row">


<div class="col s12 m9">

	<!--=========================== 項目 =============================-->

<div>
<ul class="modelList row clearfix">
	    <?php //タクソノミー・ターム条件分岐用
$term = get_term_by(
    'slug',
    get_query_var('term'),
    get_query_var('taxonomy')
);
?> 
<?php $paged = get_query_var('paged'); ?>
    <?php query_posts($query_string . '&posts_per_page=-1&paged='.$paged); ?>
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

	

	<li class="col s6 m6 box6">
		<a href="<?php the_permalink(); ?>">
					<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('workThumb', array('class' => 'responsive-img2')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_work.jpg" class="responsive-img2" />
<?php } ?></a>
	<div class="post-date work-date"><span class="small-text13"><?php the_time('Y.m.d') ?></span>
	<div class="category_ico"><?php echo get_the_term_list($post->ID,'work-cat'); ?></div>
        </div>
<h3 class="workTitle"><a href="<?php the_permalink(); ?>" class="workTitle"><?php the_title(); ?></a></h3>
<?php if( get_field('model') ): ?>
<p class="sideName">MODEL：<?php the_field('model',$post->ID); ?></p>
<?php endif; ?>
	</li>
	


<?php endwhile; ?>
</ul>

<?php else : ?>
        <p>表示する記事はありませんでした。</p>
<?php endif; ?>


<? wp_reset_query(); ?>


</div><!-- m9 -->


<div class="col s12 m3 offset-l1 rightCol">
	<?php get_sidebar('2'); ?>
</div>



</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>
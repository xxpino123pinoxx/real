
<?php
define('DEBUG_MODE','0');
define('TEST_URL','http://niveau.co.jp/backup/wp_modea/wp/');
define('MAIN_URL','http://niveau.co.jp/backup/wp_modea/');
$url = (DEBUG_MODE == "1") ? TEST_URL : MAIN_URL;
define('CMS_URL',$url);
	define('AUTHORSHIP','1'); //企業：0　個人：1
	define('META_DESC','');
	//ページ番号
	global $page;
	if ( $paged >= 2 ) { $pageno = " ページ" . $paged; }
	//robotsの制御
	if (is_home())     {$nflag = "0";}
	if (is_single())   {$nflag = "0";}
	if (is_page() )    {$nflag = "0";}
	if (is_search())   {$nflag = "1";}
	if (is_category()) {$nflag = "0";}
	if (is_month())    {$nflag = "1";}
	if (is_tag())      {$nflag = "1";}
	if (is_paged())    {$nflag = "1";}
	//T/D/K
	$title = get_title($id);
	$desc = get_desc($id);
	$keyword = get_keyword($id);

	//投稿タイプ・ターム情報取得
	$posttype =  get_post_type_object( get_post_type() )->label;
	$post_name =  get_post_type_object( get_post_type() )->name;
	$my_term   = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_name  = $my_term->name;
	$cat_now = get_the_category();
	$cat_now = $cat_now[0];
	$now_name = $cat_now->cat_name;


	switch(get_post_type()){
		// case "post":
		// $postname = "お知らせ";
		// $posturl = "news";
		// $post_en = "NEWS";
		// break;
		// case "land":
		// $postname = "土地情報";
		// $posturl = "concept/land";
		// $post_en = "LAND";
		// break;
	// case "gallery":
	// $posturl = "gallery";
	// $postname = "施工事例";
	// $post_en = "GALLERY";
	// break;
	}

	//カノニカル
	$canonical_url = "http://".$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];


	?>
	<!DOCTYPE html>
	<html lang="ja" style="margin-top:0 !important;">

	<head>
		<meta charset="UTF-8">
		<title><?=$title?><?= "".$pageno?></title>
		<?

//トップページ
if ( is_home() ) {
	$canonical_url=get_bloginfo('url')."/";
//カテゴリ
}else if (is_category()){
	$canonical_url=get_category_link(get_query_var('cat'));
//固定ページ、シングルページ
}else if (is_page() || is_single()){
    $canonical_url=get_permalink();
//ギャラリーとランド
}else if (get_post_type( $post ) == "work" || get_post_type( $post ) == "model"){
	$canonical_url=get_post_type_archive_link($post->post_type);
}

//ページングの際
if ( $paged >= 2 || $page >= 2){
	$canonical_url=$canonical_url.'page/'.max( $paged, $page ).'/';
}


?>

<link rel="canonical" href="<?php echo $canonical_url; ?>" />

<?
if ($nflag != "0"){
	global $paged, $wp_query;
	if ( !$max_page )
		$max_page = $wp_query->max_num_pages;
	if ( !$paged )
		$paged = 1;
	$nextpage = intval($paged) + 1;
	if ( null === $label )
		$label = __( 'Next Page &raquo;' );
	if ( !is_singular() && ( $nextpage <= $max_page ) )
	{
		?>
		<link rel="next" href="<?php echo next_posts( $max_page, false ); ?>" />
		<?
	}
	global $paged;
	if ( null === $label )
		$label = __( '&laquo; Previous Page' );
	if ( !is_singular() && $paged > 1  )
	{
		?>
		<link rel="prev" href="<?php echo previous_posts( false ); ?>" />
		<?
	}
}
if ($blogflag == "1"){
	?>
	<link rel="author" href="<?=$plusurl?>"/>
	<?
}
?>




<?php if(is_404()): ?>
	<meta name="robots" content="noindex,nofollow">
<?php endif; ?>
<meta name="description" content="<?=$desc?>">
<meta name="keywords" content="<?=$keyword?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />


<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if ((strpos($ua, 'iPhone') !== false) || ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false)) || (strpos($ua, 'Windows Phone') !== false)) : ?>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php else : ?>
	<meta name="viewport" content="width=1110">
<?php endif; ?>






<meta name="format-detection" content="telephone=no">

<?php if(is_single()){ // 投稿記事 ?>
<meta property='og:type' content='article'>
<meta property='og:title' content='<?php the_title() ?>'>
<meta property='og:url' content='<?php the_permalink() ?>'>
<meta property='og:description' content='<?php echo mb_substr(get_the_excerpt(), 0, 100) ?>'>
<?php } else { //ホーム・カテゴリー・固定ページなど ?>
<meta property='og:type' content='website'>
<meta property='og:title' content='<?php bloginfo('name') ?>'>
<meta property='og:url' content='<?php bloginfo('url') ?>'>
<meta property='og:description' content='<?php bloginfo('description') ?>'>
<? } ?>
<meta property='og:site_name' content='<?php bloginfo('name'); ?>'>
<?php
  if (is_single() or is_page()){//投稿記事か固定ページ
    if (has_post_thumbnail()){//アイキャッチがある場合
       $image_id = get_post_thumbnail_id();
       $image = wp_get_attachment_image_src($image_id, 'full');
       echo '<meta property="og:image" content="'.$image[0].'">';echo "\n";
    } elseif( preg_match( '/<img.*?src=(["\'])(.+?)\1.*?>/i', $post->post_content, $imgurl ) && !is_archive()) {//アイキャッチ以外の画像がある場合
       echo '<meta property="og:image" content="'.$imgurl[2].'">';echo "\n";
    } else {//画像が1つも無い場合
       echo '<meta property="og:image" content="http://niveau.co.jp/backup/wp_modea/wp/wp-content/themes/modea/img/ogp.png">';echo "\n";
    }
  } else { //ホーム・カテゴリーページなど
     echo '<meta property="og:image" content="http://niveau.co.jp/backup/wp_modea/wp/wp-content/themes/modea/img/ogp.png">';echo "\n";
  }
?>


<!-- common -->
<link href='https://fonts.googleapis.com/css?family=Montserrat|Questrial' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/materialize/css/materialize.min.css" />
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">
<link href="<?php bloginfo('template_directory'); ?>/style2.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.maximage.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.bxslider.css" />

<style>

.mask {
  -webkit-clip-path: url(#svgPath);
  clip-path: url(#svgPath);
}
</style>


<!-- common -->

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->





<?php if (in_category('news')) : ?>

<?php wp_head(); ?>

<?php elseif (is_home()||is_page('menu')||is_page('party')) : ?>

	<?php wp_deregister_script('jquery'); ?>
<!-- WordPressのjQueryを読み込ませない -->
<?php wp_head(); ?>
<?php else: ?>


	
<?php endif; ?>

<script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.1.4.min.js"></script>

<script src="<?php bloginfo('template_directory'); ?>/js/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	DD_belatedPNG.fix('img, .png-background');
});
</script>
<!-- / common -->

<!--<script type="text/javascript">
$(document).ready(function() {
	$('body').fadeIn(1500);
});
</script>-->

</head>


<?php if(is_category()||in_category()||is_home()||is_front_page()||is_single()): ?>
<body <?php body_class(); ?>>
	<?php else: ?>
	<body id="<?php echo $post->post_name ?>" <?php body_class(); ?>>
	<?php endif; ?>

  <div id="wrapper">
<!--=========================== side =============================-->

<?php if(is_mobile()) { ?>
<!--**********************【スマホ向けコンテンツの処理】**********************/-->



<!--========= sp =========-->
<header class="topCol header sp_head">

	<?php
			if ( is_front_page() && is_home() ) : ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" width="140" height="38" /></a></h1>

			<?php else : ?>
			<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" width="140" height="38" /></a></p>
			<?php endif;?>


<div class="navArea">
	

<nav id="top-head" class="nav-wrapper fixed z-depth-0">
	<a href="#" id="panel-btn" data-activates="nav-mobile" class="hide-on-med-and-up button-collapse mobile-nav-trigger"><span id="panel-btn-icon"></span></a>	

<!-- nav sp -->

      <ul id="nav-mobile" class="mincho side-nav hide-on-med-and-up fixed">
      	
        
        	<li><a class="menubtn_01" href="<?php echo esc_url( home_url( '/' ) ); ?>news" title="ニュース"><img src="<?php echo get_template_directory_uri(); ?>/img/nav01_sp.png" width="110" height="14" alt="NEWS" /><span></span></a></li>
	<li><a class="menubtn_02" href="<?php echo esc_url( home_url( '/' ) ); ?>models" title="モデル一覧"><img src="<?php echo get_template_directory_uri(); ?>/img/nav02_sp.png" width="110" height="14" alt="MODELS" /><span></span></a></li>
	<li><a class="menubtn_03" href="<?php echo esc_url( home_url( '/' ) ); ?>campaign-girl" title="キャンペーンガール"><img src="<?php echo get_template_directory_uri(); ?>/img/nav03_sp.png" width="110" height="14" alt="CAMPAIGN GIRL" /><span></span></a></li>
	<li><a class="menubtn_04" href="<?php echo esc_url( home_url( '/' ) ); ?>works" title="実績"><img src="<?php echo get_template_directory_uri(); ?>/img/nav04_sp.png" width="110" height="14" alt="WORKS" /><span></span></a></li>
	<li><a class="menubtn_05" href="<?php echo esc_url( home_url( '/' ) ); ?>contact" title="お問い合わせ"><img src="<?php echo get_template_directory_uri(); ?>/img/nav05_sp.png" width="110" height="14" alt="CONTACT" /><span></span></a></li>

			<li class="bn_mdl"><a href="<?php echo home_url( '/' ); ?>audition" title="モデル募集中!"><img src="<?php echo get_template_directory_uri(); ?>/img/topBn01.png" alt="モデル募集中" class="responsive-img" /></a></li>


					<li id="subNav01" class="subNav g-font"><a href="<?php echo home_url( '/' ); ?>company">Company</a></li>
	<li class="subNav g-font"><a href="<?php echo home_url( '/' ); ?>guideline">GuideLine</a></li>
	<li class="subNav g-font"><a href="<?php echo home_url( '/' ); ?>sitemaps">SiteMaps</a></li>
	<li class="subNav g-font"><a href="<?php echo home_url( '/' ); ?>privacy">Privacy Policy</a></li>




	<li class="icoNavArea">
      		<ul class="icoNav">
      			<li><a href="https://www.facebook.com/MODEL.MODEA/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_fb.png" alt="FACEBOOK" class="responsive-img" width="17" height="16" /></a></li>
      			<li><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_insta_top.png" alt="インスタグラム" class="responsive-img" width="16" height="16" /></a></li>

	      	</ul>
	      </li>
	<li class="footBn">
      		<ul class="footBn02">
      			<li class="L"><a href="http://www.j-m-a-a.com/policy/publicity-rights.html" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/topBn02.png" alt="守ろう！！肖像財産権について。" class="responsive-img" width="60" height="39" /></a></li>
      			<li class="R"><a href="http://www.j-m-a-a.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/topBn03_sp.png" alt="一般社団法人日本モデルエージェンシー協会" class="responsive-img" width="27" height="40" /></a></li>
	      	</ul>
	      </li>			
      </ul>

</nav>

</div><!-- navArea -->


</header><!-- topCol -->


<?php } else { ?>
<!--**********************【PC向けコンテンツの処理】**********************/-->


<header class="topCol header">


<!--========= pc =========-->

    <div id="side" class="hide-on-small-and-down">
    	<div class="sideIn">
    		<header class="topCol header">
    	<?php
			if ( is_front_page() && is_home() ) : ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" width="160" height="44" /></a></h1>

			<?php else : ?>
			<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo('name'); ?>" width="160" height="44" /></a></p>
			<?php endif;?>





<div class="navArea">
<a href="#" id="panel-btn" data-activates="nav-mobile" class="hide-on-med-and-up button-collapse mobile-nav-trigger"><span id="panel-btn-icon"></span></a>		

<nav class="nav-wrapper fixed z-depth-0">
<!-- nav pc -->
<div class="hide-on-small-only navArea">
	<div class="navAreaIn">
<ul class="hide-on-small-only global-nav" id="global-nav2">
	<li><a class="menubtn_01" href="<?php echo esc_url( home_url( '/' ) ); ?>news" title="ニュース">NEWS<span></span></a></li>
	<li <?php if(is_page('models')||is_singular('model')):?>class="current"<?php endif;?>><a class="menubtn_02" href="<?php echo esc_url( home_url( '/' ) ); ?>models" title="モデル一覧">MODELS<span></span></a></li>
	<li><a class="menubtn_03" href="<?php echo esc_url( home_url( '/' ) ); ?>campaign-girl" title="キャンペーンガール">CAMPAIGN GIRL<span></span></a></li>
	<li <?php if(is_page('works')||is_singular('work')):?>class="current"<?php endif;?>><a class="menubtn_04" href="<?php echo esc_url( home_url( '/' ) ); ?>works" title="実績">WORKS<span></span></a></li>
	<li><a class="menubtn_05" href="<?php echo esc_url( home_url( '/' ) ); ?>contact" title="お問い合わせ">CONTACT<span></span></a></li>


</ul>

<div><a href="<?php echo home_url( '/' ); ?>audition" title="モデル募集中!"><img src="<?php echo get_template_directory_uri(); ?>/img/topBn01.png" alt="モデル募集中" class="responsive-img" /></a></div>
</div><!-- navAreaIn --> 
</div><!-- navArea -->
<!-- nav sp -->


</nav>

</div><!-- navArea -->




</header><!-- topCol -->
<?php } ?>

<footer id="footer">

<ul class="footMenu">
	<li><a href="<?php echo home_url( '/' ); ?>company">Company</a></li>
	<li><a href="<?php echo home_url( '/' ); ?>guideline">GuideLine</a></li>
	<li><a href="<?php echo home_url( '/' ); ?>sitemaps">SiteMaps</a></li>
	<li><a href="<?php echo home_url( '/' ); ?>privacy">Privacy Policy</a></li>
</ul>

<ul class="footBn">
	<li id="footBn01" class="clearfix">
		<div class="L">
		<a href="https://www.facebook.com/MODEL.MODEA/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_fb.png" alt="FACEBOOK" class="responsive-img" width="17" height="16" /></a>
		</div>
		<div class="R">
		<a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_insta_top.png" alt="インスタグラム" class="responsive-img" width="16" height="16" /></a>
		</div>

	</li>
	<li id="footBn02" class="clearfix">
		<div class="L">
		<a href="http://www.j-m-a-a.com/policy/publicity-rights.html" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/topBn02.png" alt="守ろう！！肖像財産権について。" class="responsive-img" width="83" height="54" /></a>
		</div>
		<div class="R">
		<a href="http://www.j-m-a-a.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/topBn03.png" alt="一般社団法人日本モデルエージェンシー協会" class="responsive-img" width="37" height="54" /></a>
		</div>
	</li>
</ul>

<p class="copy g-font">Copyright 2016 MODEA. All rights reserved.</p>

</footer>


      
      </div><!-- sideIn -->
    </div><!-- side -->

<!--=========================== main =============================-->


<!--========= pc =========-->
    <main id="main" class="">


    	
<?php get_header(); ?>

<div class="containerAll">


	<div class="titleArea">
	<h1 class="page-title g-font">あなたがアクセスしようとしたページは、削除されたかURLが変更されています。</h1>
	</div>


	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<p>北海道・札幌のモデル事務所 モーディアのホームページをご覧いただき、ありがとうございます。<br />大変申し訳ありませんが、あなたがアクセスしようとしたページは、削除されたかURLが変更されています。<br />お手数ですが、以下の方法でもう一度目的のページをお探しください。</p>

	<div class="row clearfix">
	<div class="col s12 m6">
		
		<h3 class="name_title mb00">◇ 在籍モデルを見る</h3>
<div class="btnArea">
<a href="<?php echo home_url( '/' ); ?>models" class="btn_beta-yg arrow arrow_arrowR">詳しくはこちら</a>
</div>

	</div>
	<div class="col s12 m6">
		
		<h3 class="name_title mb00">◇ 実績を見る</h3>

		<div class="btnArea">
<a href="<?php echo home_url( '/' ); ?>works" class="btn_beta-yg arrow arrow_arrowR">詳しくはこちら</a>
</div>


	
	</div>


</div>
	</div>



</div><!-- containerAll -->
	
<?php get_footer(); ?>

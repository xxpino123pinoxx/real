<?php
define('DEBUG_MODE','0');
define('TEST_URL','http://www.preciousjapan.net/wp/');
define('MAIN_URL','http://www.preciousjapan.net/');
$url = (DEBUG_MODE == "1") ? TEST_URL : MAIN_URL;
define('CMS_URL',$url);
	define('AUTHORSHIP','1'); //企業：0　個人：1
	define('META_DESC','');
	//ページ番号
	global $page;
	if ( $paged >= 2 ) { $pageno = " ページ" . $paged; }
	//robotsの制御
	if (is_home())     {$nflag = "0";}
	if (is_single())   {$nflag = "0";}
	if (is_page() )    {$nflag = "0";}
	if (is_search())   {$nflag = "1";}
	if (is_category()) {$nflag = "0";}
	if (is_month())    {$nflag = "1";}
	if (is_tag())      {$nflag = "1";}
	if (is_paged())    {$nflag = "1";}
	//T/D/K
	$title = get_title($id);
	$desc = get_desc($id);
	$keyword = get_keyword($id);

	//投稿タイプ・ターム情報取得
	$posttype =  get_post_type_object( get_post_type() )->label;
	$post_name =  get_post_type_object( get_post_type() )->name;
	$my_term   = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_name  = $my_term->name;
	$cat_now = get_the_category();
	$cat_now = $cat_now[0];
	$now_name = $cat_now->cat_name;


	switch(get_post_type()){
		// case "post":
		// $postname = "お知らせ";
		// $posturl = "news";
		// $post_en = "NEWS";
		// break;
		// case "land":
		// $postname = "土地情報";
		// $posturl = "concept/land";
		// $post_en = "LAND";
		// break;
	// case "gallery":
	// $posturl = "gallery";
	// $postname = "施工事例";
	// $post_en = "GALLERY";
	// break;
	}

	//カノニカル
	$canonical_url = "http://".$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	?>

		<!DOCTYPE html>
	<?php if (is_page('calendar')) : ?><html lang="ja" style="margin-top:0 !important;"><?php else: ?><html lang="ja"><?php endif; ?>

	<head>
		<meta charset="UTF-8">
		<title><?=$title?><?= "".$pageno?></title>

<link rel="canonical" href="<?php echo $canonical_url; ?>" />

<?
if ($nflag != "0"){
	global $paged, $wp_query;
	if ( !$max_page )
		$max_page = $wp_query->max_num_pages;
	if ( !$paged )
		$paged = 1;
	$nextpage = intval($paged) + 1;
	if ( null === $label )
		$label = __( 'Next Page &raquo;' );
	if ( !is_singular() && ( $nextpage <= $max_page ) )
	{
		?>
		<link rel="next" href="<?php echo next_posts( $max_page, false ); ?>" />
		<?
	}
	global $paged;
	if ( null === $label )
		$label = __( '&laquo; Previous Page' );
	if ( !is_singular() && $paged > 1  )
	{
		?>
		<link rel="prev" href="<?php echo previous_posts( false ); ?>" />
		<?
	}
}
if ($blogflag == "1"){
	?>
	<link rel="author" href="<?=$plusurl?>"/>
	<?
}
?>

<?php if(is_404()): ?>
	<meta name="robots" content="noindex,nofollow">
<?php endif; ?>
<meta name="description" content="<?=$desc?>">
<meta name="keywords" content="<?=$keyword?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if ((strpos($ua, 'iPhone') !== false) || ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false)) || (strpos($ua, 'Windows Phone') !== false)) : ?>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php else : ?>
	<meta name="viewport" content="width=1110">
<?php endif; ?>
<meta name="format-detection" content="telephone=no">

<!-- common -->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/base.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/materialize/css/materialize.css" />
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">
<!-- common -->

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/instafeed.min.js"></script>
<!-- / common -->

<script type="text/javascript">
$(document).ready(function() {
	$('body').fadeIn(2000);
});
</script>

<?php if (is_page('calendar')) : ?>
	<?php wp_head(); ?>
<?php endif; ?>
</head>

<?php if ( is_category()||is_single()||is_home() || is_front_page() ) : ?>
<body <?php body_class('lighten-5'); ?>>
	<?php else: ?>
<body id="<?php echo $post->post_name ?>" <?php body_class('lighten-5'); ?>>
	
	<?php endif; ?>


<?php if ( is_home() || is_front_page() ) : ?>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
		<?php endif; ?>

<div class="wrapper">

	<header id="header" class="post-header center-align mw">
		<div class="headerIn">



<!-- global-nav -->
<nav class="nav-wrapper fixed z-depth-0" id="top-head">
	<div class="row top-headIn">

	<div class="top-logo col s12 m6 no-padding">


<?php if(is_mobile()) { ?>
<!--**********************【スマホ向けコンテンツの処理】**********************/-->

<p><a href="<?php bloginfo('url'); ?>"><img class="responsive-img fadein" id="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="ロゴ" width="200" height="64" /></a></p>

<?php } else { ?>
<!--**********************【PC向けコンテンツの処理】**********************/-->

		<div class="L">
		<p><a href="<?php bloginfo('url'); ?>"><img class="responsive-img fadein" id="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="ロゴ" width="200" height="64" /></a></p>
		</div>
		<div class="R">
			<?php if ( is_home() || is_front_page() ) : ?>
			<h1 class="desc hide-on-small-only">美しく健やかな生活をもたらす本物の品質</h1>
		<?php else: ?>
		<p class="desc hide-on-small-only">美しく健やかな生活をもたらす本物の品質</p>
	<?php endif; ?>

		</div>

<?php } ?>








	</div><!-- top-logo -->


	<div class="col s12 m6 no-padding hide-on-small-only rightMenu">
		<div class="col m7">
			<p class="tel-link lato-font"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_telS.png" width="22" height="22" class="mr10" />011-790-6557<br /></p>

		</div>

		<div class="col m5 no-padding">
			<a href="<?php echo home_url( '/' ); ?>contact" class="btn_blackLine" title="お問い合わせ">お問い合わせ・ご購入</a>

		</div>

	</div><!-- col -->

	</div><!-- row -->




	<a href="#" id="panel-btn" data-activates="nav-mobile" class="hide-on-med-and-up button-collapse mobile-nav-trigger"><span id="panel-btn-icon"></span></a>		

<!-- nav pc -->
<ul class="hide-on-small-only rightNav" id="global-nav2">
			<li><a href="<?php echo home_url( '/' ); ?>information" title="お知らせ">お知らせ</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>products" title="商品一覧">商品一覧</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>about" title="水素について">水素について</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>voice" title="お客様の声">お客様の声</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>faq" title="よくある質問">よくある質問</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>guide" title="ご利用ガイド">ご利用ガイド</a></li>
</ul>

<!-- nav sp -->

      <ul id="nav-mobile" class="mincho side-nav hide-on-med-and-up fixed">
        
        	<li><a href="<?php echo home_url( '/' ); ?>information" title="お知らせ">お知らせ</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>products" title="商品一覧">商品一覧</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>about" title="水素について">水素について</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>voice" title="お客様の声">お客様の声</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>faq" title="よくある質問">よくある質問</a></li>
			<li><a href="<?php echo home_url( '/' ); ?>guide" title="ご利用ガイド">ご利用ガイド</a></li>
      </ul>

</nav>

			<!-- /.head -->
		</div><!-- headerIn -->
		<!-- /#header --></header>

		<section id="container" class="mw">	
			<div id="pageHead">

				<?php if ( !is_home() || !is_front_page() ) : ?>


				<?php if ( is_singular('product') ) : ?>

				<div class="post-header2">

					<?php else: ?>

				<div class="post-header2" style="<?php
					if (has_post_thumbnail() )	{
						echo 'background-image:url(';
						$image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
						echo $image_url[0];
						echo ')';
					} else {
						//echo 'http://niveau.co.jp/backup/wp_komoriku/wp/wp-content/themes/hp-dap_03/img/bg_white.png';
						

					} ?>">

					<?php endif; ?>

					<div class="post-header-inner">
						<div class="container">


<!--=========================== about =============================-->
<?php if(is_page('about')): ?>
<h1 class="page-title page-title_about mincho"><?php the_title();?><span><?php the_field('en-title',$post->ID); ?></span></h1>


<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; <?php the_title();?></p>

<!--=========================== other =============================-->
<?php else : ?>

<div class="row clearfix">
	<div class="col s12 m8">
						<?php if(is_page()): ?>

<h1 class="page-title mincho"><?php the_title();?><span><?php the_field('en-title',$post->ID); ?></span></h1>

					<?php else : ?> 
					<?php $post = $posts[0]; ?>
					<?php if (is_category()) { ?>
					<h1 class="page-title mincho"><?php single_cat_title(); ?><span>
						<?php
  $cat_id = get_queried_object()->cat_ID;
  $post_id = 'category_'.$cat_id;
  ?>
  <?php the_field('en-title',$post_id); ?></span></h1>

					<?php } elseif( is_tag() ) { ?>
					<h1 class="page-title mincho">Tag &#8216;<?php single_tag_title(); ?>&#8217;</h1>

					<?php } elseif (is_day()) { ?>
					<h1 class="page-title mincho"><?php the_time(__('Y.n.j', '')); ?></h1>

					<?php } elseif (is_month()) { ?>
					<h1 class="page-title mincho"><?php the_time(__('Y.n', '')); ?></h1>

					<?php } elseif (is_year()) { ?>
					<h1 class="page-title mincho"><?php the_time(__('Y', '')); ?></h1>

					<?php } elseif (is_author()) { ?>
					<h1 class="page-title mincho">Author Archive</h1>

					<?php } elseif (in_category('information')) { ?>
					<h1 class="page-title mincho">お知らせ<span>Information</span></h1>


					<?php } elseif (is_singular('product')) { ?>
					<p class="page-title mincho">商品一覧<span>Product lineup</span></p>


					<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>

					<h1 class="page-title mincho">Archives</h1>
					<?php } elseif (is_post_type_archive( $post_types )) { ?>
					<h1 class="page-title mincho"><?php post_type_archive_title(); ?></h1>

					<?php } elseif( is_404() ) { ?>
					<h1 class="page-title mincho">お探しのページが見つかりません<span>Not found</span></h1>

					<?php } elseif (is_home() && !is_paged()) { ?>
					<?php } else { ?>
					<h1 class="page-title mincho"><?php the_title();?></h1>
					<?php } ?>
				<?php endif; ?>
</div><!-- col -->
				<!-- breadcrumbs -->
				<div class="breadcrumbArea col s12 m4">
				<?php if(!is_front_page()): ?>

				<?php if(is_singular('product')): ?>
				<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; <a href="<? bloginfo('url') ?>/products">商品一覧</a> &gt; <?php the_title(); ?></p>

				<?php elseif(in_category('information')): ?>
				<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; <a href="<? bloginfo('url') ?>/information">お知らせ</a> &gt; <?php the_title(); ?></p>

			<?php elseif(is_tax()): ?>
			<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; <a href="<? bloginfo('url') ?>/<?=$posturl ?>"><?=$postname ?></a> &gt; <?=$term_name ?><?=$now_name ?>bbbb</p>

		<?php elseif(is_category()): ?>
		<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; <?php single_cat_title(); ?></p>

	<?php elseif(is_404()): ?>
	<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; 404</p>

	<?php else: ?>
	<p class="breadcrumbs small-text" itemprop="breadcrumb"><a href="<?php echo home_url( '/' ); ?>" class="home">ホーム</a> &gt; <?php the_title();?></p>
<?php endif; ?>
<?php endif; ?>
</div><!-- breadcrumbArea -->
<!-- breadcrumbs -->

</div><!-- row -->

<?php endif; ?>

</div><!-- container -->

</div><!-- post-header-inner -->
</div><!-- post-header -->
<?php endif; ?>


<!-- /#pageHead --></div>


	
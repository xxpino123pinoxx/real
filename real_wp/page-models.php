<?php get_header(); ?>

<div class="containerAll">




	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



<div class="container100p2">

<!--=========================== 項目 =============================-->

<?php
$args = array(
    'hideempty' => 1
);
$term_objs = get_terms('model-cat',$args);
foreach ($term_objs as $term_obj) :
?>

<h2 class="uLine cat g-font"><?php echo esc_html($term_obj->name); ?></h2>



<div>
	

<?php
    $args = array (
        'post_type'   => 'model',
        'model-cat'     => $term_obj->slug,
        'numberposts' => -1,
		'orderby' => 'date',
		'order'   => 'ASC',
    );
    $myposts = get_posts($args);
    echo '<ul class="modelList row clearfix">';
    foreach ($myposts as $post) :
        setup_postdata($post);
?>	

	<li class="col s6 m4 box5">
			<div>
				<a href="<?php the_permalink(); ?>" class="triangle">

			<?php
	if(get_field('new_fase') == "new")
	{
	    //test1 の処理
	    echo '<img src="http://www.modea.co.jp/wp/wp-content/themes/modea/img/ico_new.png" width="74" height="20" class="ico_new" />';
	}
	?>


			<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('modelThumb', array('class' => 'responsive-img2', 'width' => '420', 'height' => '279')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_model.jpg" width="420" height="279" class="responsive-img2" />
<?php } ?>
</a>
		<h3 class="name_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?><span class="name_subTitle"><?php the_field('name',$post->ID); ?></span></a>

			<?php if( get_field('instagram') ):?>
		<span class="insta"><a href="https://www.instagram.com/<?php the_field('instagram',$post->ID); ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico_insta.png" width="24" height="24" /></a></span><?php endif; ?>

	</h3>
		</div>
	</li>
	
<?php
		endforeach;
		echo '</ul>'; ?>
		
	<?php
	endforeach;
	wp_reset_postdata();
	?>
</div>



	</div><!-- post -->
</div><!-- container100p2 -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>

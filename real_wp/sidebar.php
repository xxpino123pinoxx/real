

		<aside id="columnSide" class="colIn">
			

<h3 class="side_title g-font"><img src="<?php bloginfo('template_url'); ?>/img/sidetitle_01.png" alt="RECENT POSTS" width="100" height="12" /></h3>
<ul class="sideInfo sideList">


<?php query_posts($query_string ."&cat='1'&order=DESC&posts_per_page=5"); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<!--お知らせ情報ループ-->
	<li class="clearfix">
<span class="day"><?php the_time('Y.m.d') ?></span>
<span class="category_ico">
            <?php $cats = get_the_category();
            $exclude = array(); // 不要なカテゴリのID
            foreach((array)$cats as $cat)
              if(!in_array($cat->cat_ID, $exclude))
                echo '<a href="' . get_category_link($cat->cat_ID) . '">' . $cat->cat_name . '</a>'; ?>
        </span><br />
		
<h4 class="newsTitle"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if(mb_strlen($post->post_title)>20) { $title= mb_substr($post->post_title,0,20) ; echo $title. … ;
	} else {echo $post->post_title;}?></a></h4></li>
	<!--お知らせ情報ループ-->

	<?php endwhile; ?><?php else : ?><p class="mt10">まだ記事が投稿されていません</p>
	<?php endif; ?>
	<?php query_posts($query_string); ?>

</ul>

<h3 class="side_title g-font sideCatTitle mt60"><img src="<?php bloginfo('template_url'); ?>/img/sidetitle_02.png" alt="CATEGORY" width="100" height="12" /></h3>


<ul class="sideCat sideList">
    <?php 
$categories = wp_list_categories(array(
	'title_li' =>'',
	'echo' => 0,
	'depth' => 1,
	'show_count' => 1
	));
$categories = preg_replace('/<\/a> \(([0-9]+)\)/', '</a><span class="count"><span class="countIn">\\1</span></span>', $categories );
echo $categories;
    ?>
</ul>



		<!-- /#columnSide --></aside>

<?php get_header(); ?>

<div class="containerAll">

<div class="container100p2">

<h1 class="page-title g-font"><img src="<?php bloginfo('template_url'); ?>/img/title_news.png" alt="NEWS" width="155" height="15" /></h1>
<div class="row">


<div class="col s12 m9">

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



						<h2 class="post-title">
	<span class="post-date small-text13"><?php the_time('Y.m.d') ?></span>
	<span class="category_ico">
            <?php $cats = get_the_category();
            $exclude = array(); // 不要なカテゴリのID
            foreach((array)$cats as $cat)
              if(!in_array($cat->cat_ID, $exclude))
                echo '<a href="' . get_category_link($cat->cat_ID) . '">' . $cat->cat_name . '</a>'; ?>
        </span><br />

	<span class="arTitle"><?php the_title(); ?></span>
	</h2>

<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('large', array('class' => 'responsive-img mb30')); ?>
<?php } else { ?>
<?php } ?>

<?php the_content(); ?>



						<?php endwhile; endif; ?>

				
	</div>



</div><!-- m9 -->


<div class="col s12 m3 offset-l1 rightCol">
	<?php get_sidebar(); ?>
</div>



</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>
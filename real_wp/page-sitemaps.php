<?php get_header(); ?>

<div class="containerAll">
<h1 class="page-title g-font">
    SITEMAPS
</h1>
<div class="container container2">
<div class="row sitemapList">
		<ul class="col s12 m6">
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>models">タレント一覧</a></li>
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>news">お知らせ</a></li>
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>audition">オーディション</a></li>
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>works">実績</a></li>
		</ul>
		<ul class="col s12 m6">
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>company">会社概要</a></li>	
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>privacy">個人情報保護方針</a></li>
<li class="arrow arrow_arrowL"><a href="<?php echo home_url( '/' ); ?>sitemaps">サイトマップ</a></li>
		</ul>

    </div></div><!-- container -->
    </div><!-- containerAll -->
	
<?php get_footer(); ?>

<?php get_header(); ?>

<div class="row">

<div id="spimage" class="hide-on-large-only topBgImg --sp">
<div class="topBgImg__txtBox">
<h2 class="topBgImg__heading">REAL CONNECTING</h2>
<p class="topBgImg__sub">レアルが考える、人がつながるということ</p>
<p class="topBgImg__read">点と点をつなぐように<br>
線と線をむすぶほどに<br>
人はそのつながりに縁を感じる。<br>
私たち株式会社レアルは縁を大切に<br>
タレントが持つ可能性をご提案します。</p>
</div>
</div>


<!-- <div class="btmLine"></div> -->
<div class="hide-on-med-and-down topBgImg">
	<div class="topBgImg__txtBox">
	<h2 class="topBgImg__heading">REAL CONNECTING</h2>
	<p class="topBgImg__sub">レアルが考える、人がつながるということ</p>
	<p class="topBgImg__read">点と点をつなぐように<br>
	線と線をむすぶほどに<br>
	人はそのつながりに縁を感じる。<br>
	私たち株式会社レアルは縁を大切に<br>
	タレントが持つ可能性をご提案します。</p>
	</div>
	<div class="sideNewsArea hide-on-med-and-down">
	<div class="rightColIn">
		<?php include( TEMPLATEPATH . '/sideNews.php' ); ?>
	</div><!-- rightColIn -->
</div><!-- rightCol -->

</div>

<div class="spSide rightCol hide-on-large-only">
	<div class="rightColIn">
	<?php include( TEMPLATEPATH . '/sideNews.php' ); ?>
	</div><!-- rightColIn -->
</div><!-- rightCol -->

		





</div><!-- row -->
	<?php get_footer(); ?>
<?php get_header(); ?>

<div class="containerAll">

<h1 class="page-title g-font">WORKS</h1>


<div class="container100p2">


<div class="row">


<div class="col s12 m9">

<!--=========================== 項目 =============================-->

<div>
<ul class="modelList row clearfix">
<?php
// ループ条件を設定
$args = array(
'post_type' => 'work', /* 投稿タイプを指定 */
'paged' => $paged,
'posts_per_page' => -1, // 表示件数
'order' => 'DESC',
);
?>
<?php query_posts( $args ); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<li class="col s6 m6 box6">
<a href="<?php the_permalink(); ?>" class="triangle">
<?php if( get_field('movie') ): ?>
<?php endif; ?>
<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('modelThumb', array('class' => 'responsive-img2')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_model.jpg" class="responsive-img2" />
<?php } ?>
<?php if( get_field('movie') ): ?>
<?php endif; ?>
</a>
<div class="post-date work-date"><span class="small-text13"><?php the_time('Y.m.d') ?></span>
<div class="category_ico"><?php echo get_the_term_list($post->ID,'work-cat'); ?></div>
</div>
<h3 class="workTitle">

<?php if( get_field('movie') ): ?>
<a href="<?php the_permalink(); ?>" class="workTitle">
<?php endif; ?>
<?php the_title(); ?>
<?php if( get_field('movie') ): ?>
</a>
</h3>
<?php endif; ?>
<?php if( get_field('model') ): ?>
<p class="sideName">MODEL：<?php the_field('model',$post->ID); ?></p>
<?php endif; ?>
</li>



<?php endwhile; ?>
</ul>

<?php else : ?>
<p>表示する記事はありませんでした。</p>
<?php endif; ?>


<? wp_reset_query(); ?>


</div><!-- m9 -->


<div class="col s12 m3 offset-l1 rightCol">
<?php get_sidebar('2'); ?>
</div>



</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->

<?php get_footer(); ?>
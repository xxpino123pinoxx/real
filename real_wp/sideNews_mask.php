		<section id="news" class="bgClearWhite">
			<h2 class="homeSide_title g-font"><img src="<?php bloginfo('template_url'); ?>/img/sidecat_01.png" alt="NEWS" width="59" height="13" />
				<a href="<?php echo home_url( '/' ); ?>news" class="btn_txt arrow arrow_arrowR"><span class="pl15 g-font">ALL</span></a>
			</h2>

<?php $paged = get_query_var('paged'); ?>
								<?php query_posts($query_string . '&posts_per_page=1&paged='.$paged); ?>
								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<h3 class="sideNews_title"><span class="day"><?php the_time('Y.m.d') ?></span>
	
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<div class="container100p">
			<div class="row">
				<div class="col s4">
						<?php if ( has_post_thumbnail() ) { ?>
						<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('sideNewsThumb','class=responsive-img2'); ?>
						</a>
						<?php } else { ?>
						<a href="<?php the_permalink(); ?>" class="dummy">
						<img src="<?php bloginfo('template_directory'); ?>/img/logo-dummy.jpg" class="eye-catch-dummy responsive-img2" />
						</a>
						<?php } ?>
				</div>

				<div class="col s8">
					<p><?php echo mb_substr(strip_tags($post-> post_content),0,88).'...'; ?></p>
				</div>
			</div><!-- row -->
</div><!-- container -->
		<?php endwhile; ?>


	<?php else : ?>
	<p>ただいま準備中です。もうしばらくお待ちください。</p>
	<?php endif; ?>		


		</section><!-- bgClearWhite -->

		<section id="works" class="bgClearWhite">
			<h2 class="homeSide_title g-font"><img src="<?php bloginfo('template_url'); ?>/img/sidecat_02.png" alt="WORKS" width="59" height="13" />
				<a href="<?php echo home_url( '/' ); ?>works" class="btn_txt arrow arrow_arrowR"><span class="pl15 g-font">ALL</span></a>
			</h2>
			<ul class="container100p">
				<?php
// ループ条件を設定
$args = array(
    'post_type' => 'work', /* 投稿タイプを指定 */
    'paged' => $paged,
    'posts_per_page' => 3, // 表示件数
    'order' => 'DESC',
);
?>
<?php query_posts( $args ); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

<?php if(is_mobile()) { ?>
<!--**********************【スマホ向けコンテンツの処理】**********************/-->


				<li class="row">
					<div class="col s6 m5">
						<a href="<?php the_permalink(); ?>" class="triangle">
						<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('sideWorkThumb2', array('class' => 'responsive-img2')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_work.jpg" class="responsive-img2" />
<?php } ?>
		</a>			

					</div>

					<div class="col s6 m7">
						<p><span class="day"><?php the_time('Y.m.d') ?></span><span class="catIco"><?php echo get_the_term_list($post->ID,'work-cat'); ?></span></p>
						<h3 class="sideWorks_title"><?php the_title(); ?></h3>

					</div>
				</li><!-- row -->



<?php } elseif(is_ipad()) { ?>
<!--**********************【スマホ向けコンテンツの処理】**********************/-->


				<li class="row">
					<div class="col s6 m5">
						<a href="<?php the_permalink(); ?>">
						<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('sideWorkThumb2', array('class' => 'responsive-img2')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_work.jpg" class="responsive-img2" />
<?php } ?>

<svg class="shape" version="1.1" x="0px" y="0px" viewBox="0 0 140 92">
<clipPath id="svgPath">
    <polygon points="140,92 15.038,92 0,77.033 0,0 140,0"/>
</clipPath>
</svg>
		</a>			

					</div>

					<div class="col s6 m7">
						<p><span class="day"><?php the_time('Y.m.d') ?></span><span class="catIco"><?php echo get_the_term_list($post->ID,'work-cat'); ?></span></p>
						<h3 class="sideWorks_title"><?php the_title(); ?></h3>

					</div>
				</li><!-- row -->




<?php } else { ?>
<!--**********************【PC向けコンテンツの処理】**********************/-->


				<li class="row">
					<div class="col s6 m7">
						<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('sideWorkThumb2', array('class' => 'responsive-img2 mask')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_work.jpg" class="responsive-img2 mask" />
<?php } ?>

<svg class="shape" version="1.1" x="0px" y="0px" viewBox="0 0 140 92">
<clipPath id="svgPath">
    <polygon points="140,92 15.038,92 0,77.033 0,0 140,0"/>
</clipPath>
</svg>

					

					</div>

					<div class="col s6 m5">
						<p><span class="day"><?php the_time('Y.m.d') ?></span><span class="catIco"><?php echo get_the_term_list($post->ID,'work-cat'); ?></span></p>
						<h3 class="sideWorks_title"><?php the_title(); ?></h3>

					</div>
				</li><!-- row -->
				


<?php } ?>


		<?php endwhile; ?>
</ul>

<?php else : ?>
        <li>表示する記事はありませんでした。</li>
<?php endif; ?>


<? wp_reset_query(); ?>		


		</section><!-- bgClearWhite -->

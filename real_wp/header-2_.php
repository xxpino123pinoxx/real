
<?php
define('DEBUG_MODE','0');
define('TEST_URL','http://www.modea.co.jp/wp/');
define('MAIN_URL','http://www.modea.co.jp/');
$url = (DEBUG_MODE == "1") ? TEST_URL : MAIN_URL;
define('CMS_URL',$url);
	define('AUTHORSHIP','1'); //企業：0　個人：1
	define('META_DESC','');
	//ページ番号
	global $page;
	if ( $paged >= 2 ) { $pageno = " ページ" . $paged; }
	//robotsの制御
	if (is_home())     {$nflag = "0";}
	if (is_single())   {$nflag = "0";}
	if (is_page() )    {$nflag = "0";}
	if (is_search())   {$nflag = "1";}
	if (is_category()) {$nflag = "0";}
	if (is_month())    {$nflag = "1";}
	if (is_tag())      {$nflag = "1";}
	if (is_paged())    {$nflag = "1";}
	//T/D/K
	$title = get_title($id);
	$desc = get_desc($id);
	$keyword = get_keyword($id);

	//投稿タイプ・ターム情報取得
	$posttype =  get_post_type_object( get_post_type() )->label;
	$post_name =  get_post_type_object( get_post_type() )->name;
	$my_term   = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_name  = $my_term->name;
	$cat_now = get_the_category();
	$cat_now = $cat_now[0];
	$now_name = $cat_now->cat_name;


	switch(get_post_type()){
		// case "post":
		// $postname = "お知らせ";
		// $posturl = "news";
		// $post_en = "NEWS";
		// break;
		// case "land":
		// $postname = "土地情報";
		// $posturl = "concept/land";
		// $post_en = "LAND";
		// break;
	// case "gallery":
	// $posturl = "gallery";
	// $postname = "施工事例";
	// $post_en = "GALLERY";
	// break;
	}

	//カノニカル
	$canonical_url = "http://".$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];


	?>
	<!DOCTYPE html>
	<html lang="ja" style="margin-top:0 !important;" class="entryForm">

	<head>
		<meta charset="UTF-8">
		<title><?=$title?><?= "".$pageno?></title>
		<?
/*
//トップページ
if ( is_home() ) {
	$canonical_url=get_bloginfo('url')."/";
//カテゴリ
}else if (is_category()){
	$canonical_url=get_category_link(get_query_var('cat'));
//固定ページ、シングルページ
}else if (is_page() || is_single()){
    $canonical_url=get_permalink();
//ギャラリーとランド
}else if (get_post_type( $post ) == "gallery" || get_post_type( $post ) == "land"){
	$canonical_url=get_post_type_archive_link($post->post_type);
}

//ページングの際
if ( $paged >= 2 || $page >= 2){
	$canonical_url=$canonical_url.'page/'.max( $paged, $page ).'/';
}

*/
?>
<link rel="canonical" href="<?php echo $canonical_url; ?>" />

<?
if ($nflag != "0"){
	global $paged, $wp_query;
	if ( !$max_page )
		$max_page = $wp_query->max_num_pages;
	if ( !$paged )
		$paged = 1;
	$nextpage = intval($paged) + 1;
	if ( null === $label )
		$label = __( 'Next Page &raquo;' );
	if ( !is_singular() && ( $nextpage <= $max_page ) )
	{
		?>
		<link rel="next" href="<?php echo next_posts( $max_page, false ); ?>" />
		<?
	}
	global $paged;
	if ( null === $label )
		$label = __( '&laquo; Previous Page' );
	if ( !is_singular() && $paged > 1  )
	{
		?>
		<link rel="prev" href="<?php echo previous_posts( false ); ?>" />
		<?
	}
}
if ($blogflag == "1"){
	?>
	<link rel="author" href="<?=$plusurl?>"/>
	<?
}
?>




<?php if(is_404()): ?>
	<meta name="robots" content="noindex,nofollow">
<?php endif; ?>
<meta name="description" content="<?=$desc?>">
<meta name="keywords" content="<?=$keyword?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />


<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if ((strpos($ua, 'iPhone') !== false) || ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false)) || (strpos($ua, 'Windows Phone') !== false)) : ?>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php else : ?>
	<meta name="viewport" content="width=1110">
<?php endif; ?>


<script>
window.addEventListener("orientationchange", function(){
    var deg;
    switch (window.orientation) {
        case -90:
            deg = 90;  break;
        case 0:
            deg = 0;   break;
        case 90:
            deg = -90; break;
        case 180:
            deg = 180; break;
    }
    document.documentElement.style.transform = "rotate(" + deg + "deg)";
}, false);
</script>



<meta name="format-detection" content="telephone=no">


<!-- common -->
<link href='https://fonts.googleapis.com/css?family=Montserrat|Questrial' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/materialize/css/materialize.min.css" />
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">


<!-- common -->

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->





<?php if (in_category('news')||is_page('entry')) : ?>

<?php wp_head(); ?>

<?php elseif (is_home()||is_page('menu')||is_page('party')) : ?>

	<?php wp_deregister_script('jquery'); ?>
<!-- WordPressのjQueryを読み込ませない -->
<?php wp_head(); ?>
<?php else: ?>


	
<?php endif; ?>

<script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.1.4.min.js"></script>


<!-- / common -->

<script type="text/javascript">
$(document).ready(function() {
	$('body').fadeIn(1500);
});
</script>

</head>


<?php if(is_category()||in_category()||is_home()||is_front_page()||is_single()): ?>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	<?php else: ?>
	<body id="<?php echo $post->post_name ?>" <?php body_class(); ?>>
	<?php endif; ?>

  <div id="wrapper">


<!--========= pc =========-->
    <main id="main" class="">

    	
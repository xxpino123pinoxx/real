<?php get_header(); ?>

<div class="containerAll">

<div class="container100p2">

<div class="row">






<div class="col s12 m12">

<div id="post-<?php the_ID(); ?>" <?php post_class('modelSingle'); ?>>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<!--ここから編集-->

<div class="clearfix modelsCatArea">
<h3 class="modelsCat">

<?php

//記事IDとタクソノミーを指定してタームを取得
$product_terms = wp_get_object_terms($post->ID, 'model-cat');

//タームを出力
if(!empty($product_terms)){
  if(!is_wp_error( $product_terms )){
    foreach($product_terms as $term){
 

      echo '' . $term->name . '';
    }
  }
}

?>

</h3>

<ul class="composite clearfix">
  <?php if( get_field('instagram') ):?>
    <li><a href="https://www.instagram.com/<?php the_field('instagram',$post->ID); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_insta.png" alt="インスタグラム" class="responsive-img ico_insta" width="24" height="24" /></a></li>
    <?php endif; ?>

    <?php if( get_field('profile') ):?>
    <li id="tooltip"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/btn_prof.png" alt="PROFILE" class="responsive-img" width="54" height="12" /></a>

<div class="tip"><?php the_field('profile',$post->ID); ?></div>

    </li>
    <?php endif; ?>

</ul>

</div>





<div class="compArea">
<?php the_content(); ?>
</div>




<?php endwhile; ?>







<div class="modelF clearfix">
<div class="L"><?php previous_post_link('%link', 'Previous'); ?></div>
<div class="back"><a href="<?php echo home_url( '/' ); ?>models" title="タレント一覧">タレント一覧へ戻る</a></div>
<div class="R"><?php next_post_link('%link', 'Next'); ?></div>
</div>





<?php else : ?>
	<?php endif; ?>			
	</div>




</div><!-- m9 -->









</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>
<?php get_header(); ?>

<div class="containerAll">

<h1 class="page-title g-font">AUDITION</h1>

<div class="container100p2">


<div class="row">


<div class="col s12">

<!--=========================== 項目 =============================-->

<div>
<ul class="modelList row clearfix">
<?php
// ループ条件を設定
$args = array(
'post_type' => 'audition', /* 投稿タイプを指定 */
'paged' => $paged,
'posts_per_page' => -1, // 表示件数
'order' => 'DESC',
);
?>
<?php query_posts( $args ); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<li class="col s6 m6 box6">
<a href="<?php the_permalink(); ?>?postID=<?php the_ID(); ?>" class="triangle">

<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('modelThumb', array('class' => 'responsive-img2')); ?>
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-dummy_model.jpg" class="responsive-img2" />
<?php } ?>
</a>
<div class="post-date work-date"><span class="small-text13"><?php the_time('Y.m.d') ?></span>
</div>
<h3 class="workTitle">

<a href="<?php the_permalink(); ?>?postID=<?php the_ID(); ?>" class="workTitle">

<?php the_title(); ?>

</a>
</h3>
<p><?php if(mb_strlen($post->post_content)>60) { $content= mb_substr($post->post_content,0,60) ; echo $content. … ;
} else {echo $post->post_content;}?></p>
</li>



<?php endwhile; ?>
</ul>

<?php else : ?>
<p>表示する記事はありませんでした。</p>
<?php endif; ?>


<? wp_reset_query(); ?>


</div><!-- m9 -->



</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->

<?php get_footer(); ?>
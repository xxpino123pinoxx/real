<?php get_header(); ?>

<div class="containerAll">

<div class="container100p2">

<h1 class="page-title g-font">NEWS</h1>

<div class="row">


<ul class="col s12 m9">

	<?php $paged = get_query_var('paged'); ?>
								<?php query_posts($query_string . '&posts_per_page=20&paged='.$paged); ?>
								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<li id="post-<?php the_ID(); ?>" class="post-section content-eye-catch">
	<div class=" row">
	<div class="post-eye-catch col s5 m3">
	<?php if ( has_post_thumbnail() ) { ?>
	<a href="<?php the_permalink(); ?>">
	<?php the_post_thumbnail('newsThumb','class=responsive-img2'); ?>
	</a>
	<?php } else { ?>
	<a href="<?php the_permalink(); ?>" class="dummy">
	<img src="<?php bloginfo('template_directory'); ?>/img/logo-dummy.jpg" class="eye-catch-dummy responsive-img2" />
	</a>
	<?php } ?>
	</div>

	<div class="col s7 m9">
	<h2 class="post-title">
	<span class="post-date small-text13"><?php the_time('Y.m.d') ?></span>
	<span class="category_ico">
            <?php $cats = get_the_category();
            $exclude = array(); // 不要なカテゴリのID
            foreach((array)$cats as $cat)
              if(!in_array($cat->cat_ID, $exclude))
                echo '<a href="' . get_category_link($cat->cat_ID) . '">' . $cat->cat_name . '</a>'; ?>
        </span><br />

	<a href="<?php the_permalink(); ?>" class="arTitle"><?php the_title(); ?></a>
	</h2>

	<?php if(is_mobile()) { ?>
	<!--**********************【スマホ向けコンテンツの処理】**********************/-->
	<?php } else { ?>
	<!--**********************【PC向けコンテンツの処理】**********************/-->

	<p class="post-excerpt hide-on-small-only">
	<?php echo mb_substr(strip_tags($post-> post_content),0,54).'...'; ?>
	</p>

	<?php } ?>



	</div>
	</div><!-- post-section -->
	</li><!-- content-item -->

	<?php endwhile; ?>

	<div class="navigation clearfix">
	<?php if(function_exists('wp_pagenavi')): ?>
	<?php wp_pagenavi(); ?>
	<?php else : ?>
	<div class="alignleft"><?php previous_posts_link(__( 'prev' )) ?></div>
	<div class="alignright"><?php next_posts_link(__( 'next' )) ?></div>
	<?php endif; ?>
	</div>

	<?php else : ?>
	<li>ただいま準備中です。もうしばらくお待ちください。</li>
	<?php endif; ?>



</ul><!-- m9 -->


<div class="col s12 m3 offset-l1 rightCol">
	<?php get_sidebar(); ?>
</div>



</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>
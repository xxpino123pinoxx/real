//materializecss regular

(function($){
  $(function(){



    var window_width = $(window).width();







    // Plugin initialization
    //$('.slider').slider({transition:3000,interval:6000,full_width: true,});
    $('.scrollspy').scrollSpy();
    $('.modal-trigger').leanModal();
    $('.button-collapse').sideNav({'edge': 'left'});
    $('select').not('.disabled').material_select();

// Windowsize only body class
function mediaQueryClass(width) {
  if(width > 992) {
    $('body').addClass('on-large-only').removeClass('on-small-only').removeClass('on-med-only');
  } else if(width > 600) {
	$('body').addClass('on-med-only').removeClass('on-small-only').removeClass('on-large-only');
  } else {
	$('body').addClass('on-small-only').removeClass('on-med-only').removeClass('on-large-only');
  }
}
mediaQueryClass($(window).width());
$(window).resize(function(){
  mediaQueryClass($(window).width());
});



  }); // end of document ready
})(jQuery); // end of jQuery name space



// スムーススクロール
jQuery(document).ready(function($) {
	$('a[href^=#]').click(function() {
		if ($(this).hasClass('scroller')) {
			var target = $(this).attr('href');
			if (target != '#') {
				target = $(target).offset().top;
				$('html,body').animate({
					scrollTop:target
				},1500,'quart');
			} else {
				$('html,body').animate({
					scrollTop:0
				},1500,'quart');
				return false;
			}
		}
	});
	
});

jQuery.easing.quart = function(x,t,b,c,d){
	return -c * ((t=t/d-1)*t*t*t -1)+b;
};



//jquery.tile.js 同じ行の高さそろえる

//ページを読み込んだ時
$(window).load(function(){
    windowSize();
});
 
//ページをリサイズした時
$(window).resize(function(){
  windowSize();
});
 
//カラムを揃える関数
function windowSize(){
  //同じ高さに
  $('.tileblock').tile();
  //上のカラム
  $('.column2 p').tile();
  //下のカラム
  var w = $(window).width();
  var x = 600;//ウインドウサイズからスクロールバーの横幅を引く
  if (w <= x) {
    $('.tile .l4').tile(2);//600px以下だったら2カラムずつ揃える
	$('.tile .m6').tile(1);//600px以下だったら1カラムずつ揃える
  } else {
    $('.tile .l4').tile(3);//600px以上だったら3カラムずつ揃える
	$('.tile .m6').tile(2);//600px以上だったら2カラムずつ揃える
  }
}
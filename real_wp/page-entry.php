<?php get_header('2'); ?>


<div class="container">


	<div class="center-align">
	<h1 class="page-title uLine g-font"><img src="<?php bloginfo('template_url'); ?>/img/title_entry.png" alt="ENTRY FORM" width="293" height="38" /></h1>
	</div>


	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php while(have_posts()): the_post(); ?>
				<?php remove_filter('the_content', 'wpautop'); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
	</div>

</div><!-- container -->
	
<?php get_footer('2'); ?>

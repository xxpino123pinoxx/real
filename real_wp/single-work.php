<?php get_header(); ?>

<div class="containerAll">

<div class="container100p2">

<div class="row">


<div class="col s12 m12">

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="container810">

<?php if( get_field('movie') ):?>
	<div class="youtube">
<iframe width="800" height="450" src="http://www.youtube.com/embed/<?php the_field('movie',$post->ID); ?>?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>
<?php endif; ?>



<div class="post-date work-date"><span class="small-text13"><?php the_time('Y.m.d') ?></span>
	<div class="category_ico">
		<?php
if ($terms = get_the_terms($post->ID, 'work-cat')) {
    foreach ( $terms as $term ) {
        echo '<span>' . esc_html($term->name) . '</span>';
    }
}
?>

</div>
        </div>
<h2 class="workTitle"><?php the_title(); ?></h2>
<?php if( get_field('model') ): ?>
<p class="sideName">MODEL：<?php the_field('model',$post->ID); ?></p>
<?php endif; ?>




<?php the_content(); ?>

</div><!-- container810 -->
<?php endwhile; ?>







<div class="modelF clearfix">
	<div class="clearfix">
<div class="L"><?php previous_post_link('%link', 'Previous'); ?></div>
<div class="R"><?php next_post_link('%link', 'Next'); ?></div>
</div>
<div class="back"><a href="<?php echo home_url( '/' ); ?>works" title="実績">実績一覧へ戻る</a></div>
</div>





<?php else : ?>
	<?php endif; ?>			
	</div>




</div><!-- m9 -->









</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>
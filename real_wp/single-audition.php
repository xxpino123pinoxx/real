<?php get_header(); ?>

<div class="containerAll">

<div class="container100p2">

<h1 class="page-title g-font">AUDITION</h1>
<div class="row">


<div class="col s12 ">

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



						<h2 class="post-title">
	<span class="post-date small-text13"><?php the_time('Y.m.d') ?></span>
	

	<span class="arTitle"><?php the_title(); ?></span>
	</h2>

<?php if ( has_post_thumbnail() ) { ?>
<?php the_post_thumbnail('large', array('class' => 'responsive-img mb30')); ?>
<?php } else { ?>
<?php } ?>

<?php the_content(); ?>



						<?php endwhile; endif; ?>

				
	</div>

<?php if( get_field('form') ): ?>
<?php echo do_shortcode( '[contact-form-7 id="26" title="オーディションフォーム"]' ); ?>
<?php endif; ?>
</div><!-- m9 -->



</div><!-- row -->
</div><!-- container100p -->
</div><!-- containerAll -->
	
<?php get_footer(); ?>
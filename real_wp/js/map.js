var map = null;
var infowindow = new google.maps.InfoWindow();
var gmarkers = [];
var i = 0;

function inicializar() {
	// 初期設定
	var option = {
		// ズームレベル
		zoom: 18,
		// 中心座標
		center: new google.maps.LatLng(43.056383157486806, 141.34709464047546),
		// タイプ (ROADMAP・SATELLITE・TERRAIN・HYBRIDから選択)
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), option);
	google.maps.event.addListener(map, "click", function() {infowindow.close();});

	// ポイントの追加
	var point = new google.maps.LatLng(43.056383157486806,141.34709464047546);
	var marker = create_maker(point, "ラ・ジョストラ／バール、ラ・タヴォロッツァ");

	//var point = new google.maps.LatLng(48.1748859, 11.1936303);
	//var marker = create_maker(point, "ラ・タヴォロッツァ");

	var point = new google.maps.LatLng(43.05639883622431, 141.34688274596328);
	var marker = create_maker(point, "ラ・ジョストラ／ジェラテリア");
}

function create_maker(latlng, html) {
	// マーカーを生成
	var marker = new google.maps.Marker({position: latlng, map: map});
	// マーカーをクリックした時の処理
	google.maps.event.addListener(marker, "click", function() {
		infowindow.setContent(html);
		infowindow.open(map, marker);
	});
	gmarkers[i] = marker;
	i++;
	return marker;
}

function map_click(i) {
	google.maps.event.trigger(gmarkers[i], "click");
}

google.maps.event.addDomListener(window, "load", inicializar);

/**
 * jQuery rollover plugin v1.0
 *
 * rollover the images that you hover and have current URL not to rollover.
 * http://www.miyagino.net/my/jquery-rollover/
 *
 * The rollover original source is as follows.
 * http://rewish.org/javascript/jquery_rollover
 *
 * This plugin is required jQuery version 1.3 or newer.
 *
 * This is under both MIT and GPL Lisences (same as jQuery).
 *
 * 2009 (c) Kazuo Kohchi <kohchi@miyagino.net>, All Rights Reserved.
 */

(function($) {
	$.fn.rollover = function(opts) {
		var default_opts = { current : '', ro : '_over.' };
		var icache = new Array();

		opts = $.extend(default_opts, opts || {});

		var image_name = function(src) {
			var m = src.match(/(.+)\.([^\.]+)$/);
			return m[1] + opts.ro + m[2];
		}

		var set_current = function(o) {
			if (o.length != 0) {
				var cimg = (o.children())[0];
				cimg.src = image_name(cimg.src);
			}
		}

		set_current(this.parent("a[href=" + opts.current + "]"));

		return this.not("[src*=" + opts.ro + "]").each(function(i) {
			var imgsrc = this.src;
			var imgsrc_ro = image_name(this.src);

			icache[i] = new Image();
			icache[i].src = imgsrc_ro;
			$(this).hover(
				function() { this.src = imgsrc_ro; },
				function() { this.src = imgsrc; }
			);
		});
	}
})(jQuery);
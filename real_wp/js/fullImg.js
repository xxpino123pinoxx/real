$(function(){
	$.ajaxSetup({cache:false});

	$('body').prepend(
		'<div id="fullbg_base"><div id="fullbg_stretch">' +
		'<img src="../img/grandopen_bg.jpg">' +
		'</div></div>'
	);

	$(window).load(function(){
		var fadeSpeed = 1000; // フェード表示させない場合は「0」

		var baseId = '#fullbg_base';
		var stretchId = '#fullbg_stretch';
		var stretchImg = $(stretchId).children('img');

		$(baseId).css({top:'0',left:'0',position:'absolute',zIndex:'-1'});
		$(stretchId).css({top:'0',left:'0',position:'fixed',zIndex:'-1',overflow:'hidden'});
		$(stretchImg).css({top:'0',left:'0',position:'absolute',opacity:'0',visibility:'visible'}).stop().animate({opacity:'1'},fadeSpeed,'linear');
		selfWH = stretchImg.width() / stretchImg.height();

		function bgAdjust(){
			var bgWidth = $(window).width(),
			bgHeight = bgWidth / selfWH;

			if(bgHeight < $(window).height()){
				bgHeight = $(window).height();
				bgWidth = bgHeight * selfWH;
			}
			$(stretchId).css({width:bgWidth,height:bgHeight});
			$(stretchImg).css({width:bgWidth,height:bgHeight});
		}
		bgAdjust();
		$(window).bind('load resize',function(){bgAdjust()});

		// コンテンツ部分の背景オーバーレイ
		var contentsHeight = $('#contents').height();
		$('#contents').prepend('<div id="overlaybg"></div>');
		$('#overlaybg').css({height:(contentsHeight),opacity:'0.3'});
	});
});
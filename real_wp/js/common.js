// スクロールで固定ヘッダー
(function($) {
    $(function() {
        var $header = $('#top-head');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 50) {
                $header.addClass('fixed');
            } else {
                $header.removeClass('fixed');
            }
        });
    });
})(jQuery);


// スムーススクロール
jQuery(document).ready(function($){
    $('a[href^=#]').click(function(){
        var speed = 1000;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});




// acordion
jQuery(document).ready(function($){
  //acordion_treeを一旦非表示に
  $(".acordion_tree").css("display","none");
  //triggerをクリックすると以下を実行
  $(".trigger").click(function(){
    //もしもクリックしたtriggerの直後の.acordion_treeが非表示なら
    if($("+.acordion_tree",this).css("display")=="none"){
         //classにactiveを追加
         $(this).addClass("active");
         //直後のacordion_treeをスライドダウン
         $("+.acordion_tree",this).slideDown("normal");
  }else{
    //classからactiveを削除
    $(this).removeClass("active");
    //クリックしたtriggerの直後の.acordion_treeが表示されていればスライドアップ
    $("+.acordion_tree",this).slideUp("normal");
  }
  });
});


//スマートフォンのみtelリンク有効

jQuery(document).ready(function($){
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0){
        $('.tel-link').each(function(){
            var str = $(this).text();
            $(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        });
    }
});


//HTMLテーブルの列にリンク先を指定できるようにする
jQuery(function($) {
  //data-hrefの属性を持つtrを選択しclassにclickableを付加
  $('th[data-href]').addClass('clickable')
  
    //クリックイベント
    .click(function(e) {
  
      //e.targetはクリックした要素自体、それがa要素以外であれば
      if(!$(e.target).is('a')){
      
        //その要素の先祖要素で一番近いtrの
        //data-href属性の値に書かれているURLに遷移する
        window.location = $(e.target).closest('th').data('href');
      };
  });
});



//bxsliderオプション
$(document).ready(function() {
      var $slider03 = $('.bxslider');
      var activeClass = 'active-slide';
      $slider03.bxSlider({
        auto: true,
        controls: false,
        randamStart: true,
        pause: 5000,
        mode: 'fade',
        speed: 1000,
        onSliderLoad: function() {
          $slider03.children('li').eq(1).addClass(activeClass);
        },
        onSlideAfter: function($slideElement, oldIndex, newIndex) {
          $slider03.children('li').removeClass(activeClass).eq(newIndex + 1).addClass(activeClass);
        }
      });
    });


//3本線のメニューボタンをパネルの開閉にあわせてバツに変化させてみる
$(function() {
  $("#panel-btn").click(function() {
    $("#panel").slideToggle(200);
    $("#panel-btn-icon").toggleClass("close");
    return false;
  });
});



//tile.jsをレスポンシブWebデザインに対応
$(function(){
  var tabWidth = 1000,
  pcWidth = 1024;

  if(pcWidth <= $(this).width()){
      $(window).load(function() {
      $('.box2').tile(2);
      $('.box5').tile(3);
      });
  }else if (tabWidth <= $(this).width()) {
    $(window).load(function() {
    $('.box2').tile(2);
    $('.box5').tile(3);
    });
  }
  $(window).resize(function(){
    if(pcWidth <= $(this).width()){
      $('.box2').tile(2);
      $('.box5').tile(3);
    }else if (tabWidth <= $(this).width()) {
      $('.box2').tile(2);
      $('.box2').tile(3);
    }else {
      $('.box2').removeAttr('style');
      $('.box5').removeAttr('style');
    }
  });
});



// =======================================================================
// ナビゲーションアイコン
// =======================================================================

// オーバーレイ作成
$('#contents').prepend('<div class="overlay"></div>');

// アイコンをクリックしたら
$('.navBtn').click(function() {
        $('header').toggleClass('navOpen'); // class付与/削除
        $('#wrap').toggleClass('fixed'); // コンテンツを固定/解除
        $('.overlay').toggle(); // オーバーレイ表示/非表示

        // スマホナビゲーションがヘッダーに被らないようにする
        var headerH = $('header').outerHeight();
        if ($('header').hasClass('navOpen')) {
                $('header nav').css('marginTop', headerH + 'px'); //ヘッダーの高さ分マージンを付ける
        }
});

// オーバーレイをクリックしたら
$('.overlay').click(function() {
        $(this).fadeOut(300); // オーバーレイ非表示
        $('header').removeClass('navOpen'); // class削除
        $('#wrap').removeClass('fixed'); // 固定解除
});